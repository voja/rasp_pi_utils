#!/bin/bash

#######################################################################
# Make project structure for embedded Linux training using Raspberry Pi
# Author: Sava Jakovljev
# Date: 09.02.2016.
#######################################################################

PWD=${PWD}
OUT_NAME="dev_pi"
DEV_PI_ROOT=/home/${USER}/${OUT_NAME}

# get all required packages, also update & upgrade
sudo apt-get update && sudo apt-get dist-upgrade
sudo apt-get install gawk gzip perl autoconf m4 automake libtool gettext gperf autogen flex g++ texinfo tofrodos gcc-multilib
sudo apt-get install git libncurses5-dev libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev nfs-kernel-server\
    zlib1g-dev libnfs-dev libiscsi-dev libsasl2-dev libsdl1.2-dev libseccomp-dev libsnappy-dev libssh2-1-dev

sudo apt-get install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev git-email\
    libaio-dev libbluetooth-dev libbrlapi-dev libbz2-dev\
    libcap-dev libcap-ng-dev libcurl4-gnutls-dev libgtk-3-dev\
    libibverbs-dev libjpeg8-dev libncurses5-dev libnuma-dev\
    librbd-dev librdmacm-dev\
    libsasl2-dev libsdl1.2-dev libseccomp-dev libsnappy-dev libssh2-1-dev\
    libvde-dev libvdeplug-dev libvte-2.90-dev libxen-dev liblzo2-dev\
    valgrind xfslibs-dev libnfs-dev libiscsi-dev

# if 'dev_pi' already exist, delete it
rm -rf ${DEV_PI_ROOT}

# create project directory tree
mkdir -p ${DEV_PI_ROOT}
mkdir -p ${DEV_PI_ROOT}/toolchain
mkdir -p ${DEV_PI_ROOT}/emulator/rootfs/fs
mkdir -p ${DEV_PI_ROOT}/emulator/core
mkdir -p ${DEV_PI_ROOT}/emulator/core

cp start_qemu.sh stop_qemu.sh qemu_patch ${DEV_PI_ROOT}/emulator/core
# cp fs.img ${DEV_PI_ROOT}/emulator/rootfs
cd ${DEV_PI_ROOT}/emulator/rootfs
wget https://www.dropbox.com/s/garc6wcz19hhzcw/fs.tar.gz
tar xvf fs.tar.gz
rm fs.tar.gz

# download toolchain: arm-none-eabi
cd ${DEV_PI_ROOT}
wget https://launchpad.net/gcc-arm-embedded/5.0/5-2015-q4-major/+download/gcc-arm-none-eabi-5_2-2015q4-20151219-linux.tar.bz2
tar xvjf gcc-arm-none-eabi-5_2-2015q4-20151219-linux.tar.bz2 -C toolchain || exit 1
rm -rf *20151219*

# download and build qemu project
wget http://wiki.qemu-project.org/download/qemu-2.5.0.tar.bz2
tar xvjf *qemu*.tar* -C ${DEV_PI_ROOT}/emulator
rm -rf *qemu*.tar*
mv ${DEV_PI_ROOT}/emulator/qemu* ${DEV_PI_ROOT}/emulator/qemu-src
mkdir -p ${DEV_PI_ROOT}/emulator/qemu-src/bin/debug/native
cd ${DEV_PI_ROOT}/emulator/qemu-src/bin/debug/native
../../../configure --enable-debug
make -j4

# set variables for Linux build
TOOLCHAIN=arm-none-eabi
export CROSS_COMPILE=/home/$USER/dev_pi/toolchain/gcc-arm-none-eabi-5_2-2015q4/bin/arm-none-eabi-
export PATH=/home/$USER/dev_pi/toolchain/gcc-arm-none-eabi-5_2-2015q4/bin:$PATH

# clone official Linux for Raspberry Pi from github
git clone --depth 1 https://github.com/raspberrypi/linux.git ${DEV_PI_ROOT}/kernel || exit 1
cp ${DEV_PI_ROOT}/emulator/core/qemu_patch ${DEV_PI_ROOT}/kernel
cd ${DEV_PI_ROOT}/kernel
#git checkout -b emulator

# patch kernel code with 'qemu_patch' file, using verastile default config. It's not native Raspberry Pi build, but good enough for QEMU
patch -p1 < qemu_patch || exit 1
make ARCH=arm versatile_defconfig
cat >> .config << EOF
CONFIG_CROSS_COMPILE="$TOOLCHAIN"
CONFIG_CPU_V6=y
CONFIG_ARM_ERRATA_411920=y
CONFIG_ARM_ERRATA_364296=y
CONFIG_AEABI=y
CONFIG_OABI_COMPAT=y
CONFIG_PCI=y
CONFIG_SCSI=y
CONFIG_SCSI_SYM53C8XX_2=y
CONFIG_BLK_DEV_SD=y
CONFIG_BLK_DEV_SR=y
CONFIG_DEVTMPFS=y
CONFIG_DEVTMPFS_MOUNT=y
CONFIG_TMPFS=y
CONFIG_INPUT_EVDEV=y
CONFIG_EXT3_FS=y
CONFIG_EXT4_FS=y
CONFIG_VFAT_FS=y
CONFIG_NLS_CODEPAGE_437=y
CONFIG_NLS_ISO8859_1=y
CONFIG_FONT_8x16=y
CONFIG_LOGO=y
CONFIG_VFP=y
CONFIG_CGROUPS=y
CONFIG_MMC_BCM2835=y
CONFIG_MMC_BCM2835_DMA=y
CONFIG_DMADEVICES=y
CONFIG_DMA_BCM2708=y
CONFIG_FHANDLE=y
EOF

make -j 4 -k ARCH=arm menuconfig
make -j 4 -k ARCH=arm bzImage
cp ${DEV_PI_ROOT}/kernel/arch/arm/boot/zImage ${DEV_PI_ROOT}/emulator/core
cd ${DEV_PI_ROOT}/emulator/core
mv zImage kernel-qemu
cd ${DEV_PI_ROOT}/emulator

echo "Build finished, setting config files..."
sudo service nfs-kernel-server stop
cat /etc/exports > exports
echo "/home/$USER/dev_pi/emulator/rootfs/fs 127.0.0.1(rw,no_subtree_check,no_root_squash,insecure)" >> temp
sed '$!N; /^\(.*\)\n\1$/!P; D' temp > exports
rm temp

sudo rm -f /etc/exports
sudo mv exports /etc

sudo mount -o loop ${DEV_PI_ROOT}/emulator/rootfs/fs.img ${DEV_PI_ROOT}/emulator/rootfs/fs
cd ${DEV_PI_ROOT}/emulator/rootfs
touch temp
sudo sed -i 's/\/dev\/mmc/#\/dev\/mmc/g' fs/etc/fstab
sudo sed 's/^/#/' fs/etc/ld.so.preload > temp
sudo rm -f fs/etc/ld.so.preload
mv temp ld.so.preload
sudo mv ld.so.preload fs/etc
cd ${DEV_PI_ROOT}/emulator/rootfs
sudo umount fs
cd ${DEV_PI_ROOT}
