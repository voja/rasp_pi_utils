#!/bin/bash

sudo mount -o loop ../rootfs/fs.img ../rootfs/fs

sudo service nfs-kernel-server start &&
./../qemu-src/bin/debug/native/arm-softmmu/qemu-system-arm -kernel kernel-qemu -cpu arm1176 -m 256 -M versatilepb -no-reboot -append "root=/dev/nfs nfsroot=10.0.2.2:/home/$USER/dev_pi/emulator/rootfs/fs ip=10.0.2.15::10.0.2.1:255.255.255.0 console=tty1" -sdl
